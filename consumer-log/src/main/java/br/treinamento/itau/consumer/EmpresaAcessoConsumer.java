package br.treinamento.itau.consumer;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import br.itau.treinamento.empresa.model.EmpresaEvento;

@Component
public class EmpresaAcessoConsumer {
	
    Logger logger = LoggerFactory.getLogger(EmpresaAcessoConsumer.class);

	
	@Autowired
	EmpresaAcessoService AcessoService;
	
    @KafkaListener(topics = "spec4-fabiana-hisako-3", groupId = "1")
	public void recebeEvento(@Payload EmpresaEvento empresaEvento) throws IOException {
    	
    	logger.info("Recebendo evento: " + empresaEvento.getCnpj());
    	
    	AcessoService.criaEventoLog(empresaEvento);
	}

}
