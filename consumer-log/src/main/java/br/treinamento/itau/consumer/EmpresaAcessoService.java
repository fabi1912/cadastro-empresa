package br.treinamento.itau.consumer;

import java.io.BufferedWriter;
import java.io.FileWriter;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import br.itau.treinamento.empresa.model.EmpresaEvento;

@Service
public class EmpresaAcessoService {
	
    Logger logger = LoggerFactory.getLogger(EmpresaAcessoService.class);

	
	public void criaEventoLog(EmpresaEvento empresaEvento) {
		
		try {
			BufferedWriter writer =  new BufferedWriter(new FileWriter("./cadastro-eventos.csv", true));	
			
			CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT);
	         		
			 csvPrinter.printRecord(
					 empresaEvento.getCnpj().toString(),
					 empresaEvento.getNome(),
					 empresaEvento.getAceito());
			 
	         csvPrinter.flush();  
	         csvPrinter.close();
		}catch(Exception e) {
			logger.error("Erro ao criar evento", e);
		}
		 

	}

}
