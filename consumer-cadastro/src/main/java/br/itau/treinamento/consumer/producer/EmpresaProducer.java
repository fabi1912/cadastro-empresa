package br.itau.treinamento.consumer.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import br.itau.treinamento.empresa.model.EmpresaEvento;

@Service
public class EmpresaProducer {
	
	@Autowired
    private KafkaTemplate<String,EmpresaEvento> empresaProducer;
	
	public void salvaEvento(EmpresaEvento empresaEvento) {
		empresaProducer.send("spec4-fabiana-hisako-3", empresaEvento);
	}
	
}
