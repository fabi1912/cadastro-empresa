package br.itau.treinamento.consumer.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Empresa {
	
	@Id
	@Column(name="empresa_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="empresa_cnpj")
	@NotNull
	private Long cnpj;
	
	@Column(name="empresa_faturamento")
	private BigDecimal faturamento;
	
	@Column(name="empresa_nome")
	private String nome;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getCnpj() {
		return cnpj;
	}
	public void setCnpj(Long cnpj) {
		this.cnpj = cnpj;
	}
	public BigDecimal getFaturamento() {
		return faturamento;
	}
	public void setFaturamento(BigDecimal faturamento) {
		this.faturamento = faturamento;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	

}
