package br.itau.treinamento.consumer.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "faturamento", url = "https://www.receitaws.com.br/v1/cnpj/")
public interface FaturamentoClient {
	
	
	@GetMapping("/{cnpj}")
    Faturamento getFaturamento(@PathVariable Long cnpj);

}
