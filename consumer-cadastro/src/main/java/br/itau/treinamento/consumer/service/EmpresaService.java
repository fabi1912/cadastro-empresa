package br.itau.treinamento.consumer.service;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.itau.treinamento.consumer.client.Faturamento;
import br.itau.treinamento.consumer.client.FaturamentoClient;
import br.itau.treinamento.consumer.model.Empresa;
import br.itau.treinamento.consumer.producer.EmpresaProducer;
import br.itau.treinamento.consumer.repository.EmpresaRepository;
import br.itau.treinamento.empresa.model.EmpresaEvento;
import br.itau.treinamento.empresa.model.EmpresaMapper;

@Service
public class EmpresaService {

	Logger logger = LoggerFactory.getLogger(EmpresaService.class);
	
	@Autowired
	private EmpresaRepository empresaRepository;
	
	@Autowired
	private FaturamentoClient faturamentoClient;
	
	@Autowired
	private EmpresaProducer empresaProducer;
	
	
	public void salvaEmpresa(Empresa empresa) {
		
		Faturamento faturamento = faturamentoClient.getFaturamento(empresa.getCnpj());
		boolean aceito=false;
		
		logger.info("faturamento do cliente "  + faturamento.getCapitalSocial());
		
		
		if(faturamento!=null && faturamento.getCapitalSocial() !=null ) {
			
			BigDecimal valorFaturamento = new BigDecimal(faturamento.getCapitalSocial());
			if(valorFaturamento.compareTo(new BigDecimal("1000000000"))==1) {
				empresa.setFaturamento(valorFaturamento);
				empresaRepository.save(empresa);
				aceito= true;
			}
		}
		
		EmpresaEvento empresaEvento = EmpresaMapper.toEmpresaEvento(empresa, aceito);
		empresaProducer.salvaEvento(empresaEvento);
		
	}
	
}
