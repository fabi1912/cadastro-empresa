package br.itau.treinamento.consumer.repository;

import org.springframework.data.repository.CrudRepository;

import br.itau.treinamento.consumer.model.Empresa;


public interface EmpresaRepository extends CrudRepository<Empresa, Long>{
	

}
