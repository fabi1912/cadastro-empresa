package br.itau.treinamento.consumer;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import br.itau.treinamento.consumer.service.EmpresaService;
import br.itau.treinamento.empresa.model.EmpresaCadastro;
import br.itau.treinamento.empresa.model.EmpresaMapper;

@Component
public class CadastroConsumer {
	
    Logger logger = LoggerFactory.getLogger(CadastroConsumer.class);

	
	@Autowired
	private EmpresaService empresaService;
	
    @KafkaListener(topics = "spec4-fabiana-hisako-2", groupId = "1")
	public void recebeEvento(@Payload EmpresaCadastro empresaCadastro) throws IOException {
    	
    	logger.info("Recebendo cadastro : " + empresaCadastro.getCnpj());
    	
    	empresaService.salvaEmpresa(EmpresaMapper.toEmpresa(empresaCadastro));
	}

}
