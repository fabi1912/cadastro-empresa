package br.itau.treinamento.empresa.model;

import br.itau.treinamento.consumer.model.Empresa;

public class EmpresaMapper {
	
	
	public static Empresa toEmpresa(EmpresaCadastro empresaCadastro) {
		Empresa empresa = new Empresa();
		empresa.setCnpj(empresaCadastro.getCnpj());
		empresa.setNome(empresaCadastro.getNome());
		return empresa;
	}
	
	
	public static EmpresaEvento toEmpresaEvento(Empresa empresa, boolean aceito) {
		EmpresaEvento empresaEvento = new EmpresaEvento();
		empresaEvento.setCnpj(empresa.getCnpj());
		empresaEvento.setNome(empresa.getNome());
		empresaEvento.setAceito(aceito);
		return empresaEvento;
	}

}
