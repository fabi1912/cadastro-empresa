package br.itau.treinamento.empresa.model;


public class EmpresaCadastro {

	
	private Long cnpj;
	private String nome;
	public Long getCnpj() {
		return cnpj;
	}
	public void setCnpj(Long cnpj) {
		this.cnpj = cnpj;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
	
}
