package br.itau.treinamento.empresa.model.dto;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;


public class EmpresaRequest {

	@NotNull
	private Long cnpj;
	private String nome;
	
	
	public Long getCnpj() {
		return cnpj;
	}
	public void setCnpj(Long cnpj) {
		this.cnpj = cnpj;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
	public EmpresaRequest(@NotNull Long cnpj, String nome, BigDecimal faturamento) {
		super();
		this.cnpj = cnpj;
		this.nome = nome;
	}
	
	public EmpresaRequest() {
		super();
	}
	
	

}
