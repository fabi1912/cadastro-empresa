package br.itau.treinamento.empresa.model.dto;

import br.itau.treinamento.empresa.model.EmpresaCadastro;

public class EmpresaMapper {
	
	
	public static EmpresaCadastro toEmpresaCadastro(EmpresaRequest empresaRequest) {
		
		EmpresaCadastro empresaCadastro= new EmpresaCadastro();
		empresaCadastro.setCnpj(empresaRequest.getCnpj());
		empresaCadastro.setNome(empresaRequest.getNome());
		return empresaCadastro;
	}

}
