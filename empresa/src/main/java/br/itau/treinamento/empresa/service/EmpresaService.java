package br.itau.treinamento.empresa.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.itau.treinamento.empresa.model.EmpresaCadastro;
import br.itau.treinamento.empresa.producer.EmpresaProducer;

@Service
public class EmpresaService {

	Logger logger = LoggerFactory.getLogger(EmpresaService.class);
	
	@Autowired
	private EmpresaProducer empresaProducer;
	
	public void salvaEmpresa(EmpresaCadastro empresaCadastro) {
		
		empresaProducer.salvaCadastro(empresaCadastro);
	
	}
	
}
