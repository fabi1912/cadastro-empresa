package br.itau.treinamento.empresa.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import br.itau.treinamento.empresa.model.EmpresaCadastro;

@Service
public class EmpresaProducer {
	
	@Autowired
    private KafkaTemplate<String,EmpresaCadastro> empresaProducer;
	
	public void salvaCadastro(EmpresaCadastro empresaCadastro) {
		empresaProducer.send("spec4-fabiana-hisako-2", empresaCadastro);
	}
	
}
