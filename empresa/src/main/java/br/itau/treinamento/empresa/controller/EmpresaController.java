package br.itau.treinamento.empresa.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.itau.treinamento.empresa.model.dto.EmpresaMapper;
import br.itau.treinamento.empresa.model.dto.EmpresaRequest;
import br.itau.treinamento.empresa.service.EmpresaService;

@RestController
@RequestMapping("/empresa")
public class EmpresaController {
	
	Logger logger = LoggerFactory.getLogger(EmpresaController.class);

	
	@Autowired
	private EmpresaService empresaService;
	
	@PostMapping
	public void salvaEmpresa(@Valid @RequestBody EmpresaRequest empresaRequest) {
		
		logger.info("Criando empresa " + empresaRequest.getCnpj() );
		empresaService.salvaEmpresa(EmpresaMapper.toEmpresaCadastro(empresaRequest));
	
	}

}
